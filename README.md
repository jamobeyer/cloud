Hochschule Flensburg
Software Engineering I
Projekt: Cloud

Diese Projekt ermöglicht es, Ordner strukturen zu erstellen. Dafür stehen alle 
CRUD- Operationen zur Verfügung. Die angelegten Ordner werden in einer json 
gespeichert. Zu finden unter build/libs.


CURL Befehle:


CREATE: curl -X POST localhost:8080/createFolder -H 'Content-type:application/json' -d name=Test

DELETE: curl -X DELETE localhost:8080/delete/0


Darstellen der Ordner unter:

localhost:8080/folders

Darstellen bestimmter Anzahl

localhost:8080/folders/1
