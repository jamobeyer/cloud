package de.flensburg.resty;

import java.time.LocalDate;

import org.json.simple.JSONObject;

public class Folder {

    private final String name;
    private LocalDate createAt;

    public Folder(String name) {
        this.name = name;
        this.createAt = LocalDate.now();
    }

    public String getFolderName() {
        return this.name;
    }

    public LocalDate getFolderDate() {
        return this.createAt;
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("folderName", name);
        json.put("folderDate", createAt.toString());

        return json.toString(); // or whatever you wish to
                                // display
    }
}