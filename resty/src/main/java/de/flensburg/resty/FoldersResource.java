package de.flensburg.resty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import de.flensburg.resty.Persistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FoldersResource implements Persistence {

    private ArrayList<Folder> ArrayFolders = new ArrayList<>();

    // Get
    @Autowired
    private Persistence<Folder> persistence;

    @GetMapping(value = "/folders")
    List<Folder> getFolders(@RequestParam(required = false, defaultValue = "0") int limit) throws IOException {
        List folders = persistence.read();

        if (limit > 0) {
            return folders.subList(0, Math.min(folders.size(), limit));
        }

        return folders;
    }

    // Get {id}
    @RequestMapping(method = RequestMethod.GET, value = "/folder/{id}")
    public String folder(@PathVariable int id) throws IOException {
        List folders = persistence.read();

        return folders.get(id).toString();
    }

    // Delete
    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public void delete(@PathVariable int id) throws IOException {
        List folders = persistence.read();

        JSONArray array = new JSONArray();

        for (int i = 0; i < folders.size(); i++) {
            array.add(folders.get(i));
        }
        // Hinzufügen des neuen Ordners
        array.remove(id);
        // Speichern der neuen Werte
        write(array);
    }

    // Post
    @PostMapping("/createFolder")
    public void someControllerMethod(@RequestParam Map<String, String> body) throws IOException {
        // Einlesen der alten Daten
        List folders = persistence.read();

        JSONArray array = new JSONArray();

        for (int i = 0; i < folders.size(); i++) {
            array.add(folders.get(i));
        }
        // Hinzufügen des neuen Ordners
        array.add(new Folder(body.get("name")));
        // Speichern der neuen Werte
        write(array);

    }

    @Override
    public List read() throws IOException {
        // JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("folders.json")) {
            // Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray folderList = (JSONArray) obj;

            return folderList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void write(List objects) throws IOException {

        System.out.println("test");

        JSONArray array = new JSONArray();

        for (int i = 0; i < objects.size(); i++) {
            array.add(objects.get(i));
        }

        try (FileWriter file = new FileWriter("folders.json")) {

            file.write(array.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}