package de.flensburg.resty;

import java.io.IOException;
import java.util.List;

public interface Persistence<T> {
    List<T> read() throws IOException;

    void write(List<T> objects) throws IOException;
}